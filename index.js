'use strict';

function validate(meta, attributes) {
  if (Array.isArray(attributes))
    attributes.forEach((attr) => validate(meta[0], attr))
  else
    Object.keys(attributes).forEach((attr) => {
      if (meta.includes(attr)) return
      const node = meta[meta.length - 1]
      if (node && typeof node !== 'string' && node[attr])
        return validate(node[attr], attributes[attr])
      throw new Error(`attribute "${attr}" is not permitted.`)
    })
}

module.exports = validate
