'use strict';

const validate = require('../index')

const assert = require('assert')

describe('validate', function() {
  it('validates attributes', function() {
    const meta = ['id', 'errors']
    assert.doesNotThrow(() => {
      validate(meta, {id: 1, errors: {title: ['is blank']}})
    })
  })

  it('throws error', function() {
    const meta = ['id']
    assert.throws(() => {
      validate(meta, {id: 1, errors: {title: ['is blank']}})
    }, /attribute "errors" is not permitted/)
  })

  context('with nested', function() {
    it('validates nested attributes', function() {
      const meta = ['id', {
        diaries: [['title']]
      }]
      assert.doesNotThrow(() => {
        validate(meta, {
          id: 1,
          diaries: [
            {title: 'Morning tea'},
            {title: 'Xx'}
          ]
        })
      })
    })

    it('throws error', function() {
      const meta = ['id', {
        diaries: [['title']]
      }]
      assert.throws(() => {
        validate(meta, {
          id: 1,
          diaries: [
            {visible: false},
            {title: 'Xx'}
          ]
        })
      }, /attribute "visible" is not permitted/)
    })
  })
})
